<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>ĐƠN HÀNG CỦA BẠN</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 48px;
        }
.glow {
 font-family: cursive;
  font-size: 30px;
  color: #000000;
  text-align: center;
  -webkit-animation: glow 1s ease-in-out infinite alternate;
  -moz-animation: glow 1s ease-in-out infinite alternate;
  animation: glow 1s ease-in-out infinite alternate;
}
td{
    border: 1px solid rgb(0, 255, 204);
}
@-webkit-keyframes glow {
  from {
    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #e60073, 0 0 40px #e60073, 0 0 50px #e60073, 0 0 60px #e60073, 0 0 70px #e60073;
  }

  to {
    text-shadow: 0 0 20px #fff, 0 0 30px #ff4da6, 0 0 40px #ff4da6, 0 0 50px #ff4da6, 0 0 60px #ff4da6, 0 0 70px #ff4da6, 0 0 80px #ff4da6;
  }
}



    </style>
</head>
<body style="margin: 0; padding: 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border-collapse: collapse;">
    <tr>
        <td>
            <img src="https://cdn7.bigcommerce.com/s-nvmwlh3674/product_images/theme_images/shoppurple_banner_db__70348.jpg?t=1531419889"
                 height=250" width="100%" alt="http://koderShop.com" style="display: block;"/>
        </td>
    </tr>
    <tr>
        <td bgcolor="#fff" style="padding: 40px 30px 40px 30px;">
            <p class="glow"> KODER SHOP</p>
            <p><b>${customerName}</b> thân mến,</p>
            <p>Bạn đã đặt hàng thành công sản phẩm :</p>
        </td>
    </tr>
        <#list products as product>
<!--            <tr  style="padding: 40px 30px 40px 30px;">-->
                <td style="padding:10px">Tên sản phẩm:${product.getProductName()} || Số lượng:${product.getQty()}</td>
<!--            </tr>-->
        </#list>
    <tr>
        <td  style="padding: 40px 30px 40px 30px;">
            <p>Chúng tôi sẽ giao hàng sớm nhất có thể.
            Hi vọng bạn hài lòng
            với sản phẩm này! </p>
            <p>Tổng:${total}</p>
            <p>Xin cảm ơn.</p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0" style="padding: 30px 30px 30px 30px;">
            <p style="color:#fff;">Địa chỉ: ${location}</p>
            <p style="color:#fff;">Số điện thoại: ${phone}</p>
        </td>
    </tr>
</table>
</body>
</html>
