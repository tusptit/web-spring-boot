package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.common.utils.ImageUtil;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.FeedBack;
import vn.com.kodergang.shop.entity.Image;
import vn.com.kodergang.shop.entity.MessagesResponse;
import vn.com.kodergang.shop.entity.Product;
import vn.com.kodergang.shop.service.FeedBackService;
import vn.com.kodergang.shop.service.impl.ProductServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.PRODUCT)
@RestController
public class ProductController {
    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private ProductServiceImpl mProductService;

    @Autowired
    private FeedBackService feedBackService;

    @Autowired
    Environment env;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public @ResponseBody
    ResponseEntity getAllProduct() {
        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mProductService.getAllProduct());
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/insertFeedBack")
    public @ResponseBody
    ResponseEntity insertFeedBack(@RequestBody FeedBack feedBack) {
        MessagesResponse mess = new MessagesResponse();
        try {
            feedBack.setCreateTime(new Date());
            feedBackService.save(feedBack);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getAllFeedBack")
    public @ResponseBody
    ResponseEntity getFeedBackByProductId(@RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(feedBackService.getFeedBackByProductId(map));
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/getOne")
    public @ResponseBody
    ResponseEntity getOneProduct(@RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mProductService.findOne(Integer.parseInt(map.get("id").toString())));
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @RequestMapping(value = {"/insert"}, method = RequestMethod.POST)
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> insert(@RequestHeader HttpHeaders headers, @RequestBody Product product) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            List<Image> image = product.getLstImages();
            for (Image item : image) {
                ImageUtil.writeImage(item.getImageData(), item.getName(), env.getProperty("folderUpload"), Integer.parseInt(env.getProperty("thumbnailSize")));
            }
            mProductService.save(product);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/update")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> update(@RequestHeader HttpHeaders headers, @RequestBody Product product) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            List<Image> image = product.getLstImages();
            for (Image item : image) {
                ImageUtil.writeImage(item.getImageData(), item.getName(), env.getProperty("folderUpload"), Integer.parseInt(env.getProperty("thumbnailSize")));
            }
            mProductService.save(product);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }

    @PostMapping("/delete")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mProductService.delete(Integer.parseInt(map.get("id").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }
}
