package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.BlogDao;
import vn.com.kodergang.shop.entity.Blog;

import java.util.List;
@Repository("BlogDaoImpl")
public class BlogDaoImpl extends GenericDaoImpl<Blog, Integer> implements BlogDao {
    @Override
    public List<Blog> getAllBlog() {
        String sql = "Select * from Blog";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Blog.class));
    }
}