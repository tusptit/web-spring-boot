package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.OrderDetail;

import java.util.List;
import java.util.Map;

public interface OrderDetailDao extends GenericDao<OrderDetail, Integer> {
    List<OrderDetail> getOrderDetail(Map map);
}
