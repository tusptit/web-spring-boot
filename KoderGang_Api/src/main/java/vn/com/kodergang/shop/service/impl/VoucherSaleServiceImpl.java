package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.VoucherSaleDao;
import vn.com.kodergang.shop.entity.VoucherSale;
import vn.com.kodergang.shop.service.VoucherSaleService;

import java.util.List;

@Service("VoucherSaleServiceImpl")
@Transactional
public class VoucherSaleServiceImpl extends GenericServiceImpl<VoucherSale, Integer> implements VoucherSaleService {
    @Autowired
    private VoucherSaleDao dao;

    public VoucherSaleServiceImpl() {
    }

    @Autowired
    public VoucherSaleServiceImpl(@Qualifier("VoucherSaleDaoImpl") GenericDao<VoucherSale, Integer> genericDao) {
        super(genericDao);
        this.dao = (VoucherSaleDao) genericDao;
    }

    @Override
    public List<VoucherSale> getAllVoucherSale() {
        return dao.getAllVoucherSale();
    }
}
