package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.AdGroupDao;
import vn.com.kodergang.shop.dao.CategoryDao;
import vn.com.kodergang.shop.dao.repo.AdGroupRepo;
import vn.com.kodergang.shop.entity.AdGroup;
import vn.com.kodergang.shop.entity.Category;
import vn.com.kodergang.shop.service.AdGroupService;
import vn.com.kodergang.shop.service.CategoryService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("AdGroupServiceImpl")
@Transactional
public class AdGroupServiceImpl extends GenericServiceImpl<AdGroup, Integer> implements AdGroupService {

    @Autowired
    private AdGroupDao dao;

    public AdGroupServiceImpl() {
    }

    @Autowired
    public AdGroupServiceImpl(@Qualifier("AdGroupDaoImpl") GenericDao<AdGroup, Integer> genericDao) {
        super(genericDao);
        this.dao = (AdGroupDao) genericDao;
    }

    @Override
    public boolean checkExistCode(String code) {
        Map map = new HashMap() {{
            put("groupCode", code);
        }};
        return dao.checkExistCode(map);
    }

    @Override
    public List<AdGroup> getAllAdGroup() {
        return dao.getAllAdGroup();
    }
}
