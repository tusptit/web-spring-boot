package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.FeedBack;

import java.util.List;
import java.util.Map;

public interface FeedBackService extends GenericService<FeedBack, Integer> {
    List<FeedBack> getFeedBackByProductId(Map map);
}
