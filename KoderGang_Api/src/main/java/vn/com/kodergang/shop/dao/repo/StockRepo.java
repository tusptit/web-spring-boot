package vn.com.kodergang.shop.dao.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.shop.entity.Stock;

@Repository
public interface StockRepo extends CrudRepository<Stock, Integer> {
}
