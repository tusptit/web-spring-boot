package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.MessagesResponse;
import vn.com.kodergang.shop.entity.Stock;
import vn.com.kodergang.shop.service.impl.StockServiceImpl;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.STOCK)
@RestController
public class StockController {
    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private StockServiceImpl mStockService;

    @PostMapping("/getAll")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> getAllStock(@RequestHeader HttpHeaders headers) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);

        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mStockService.getAllStock());
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/insert")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> insert(@RequestHeader HttpHeaders headers, @RequestBody Stock stock) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            if (mStockService.checkExistCode(stock.getStockCode())) {
                mess.setStatus(Constant.RESPONSE_STATUS.ERROR);
                return new ResponseEntity(mess, HttpStatus.OK);
            } else {
                mStockService.save(stock);
                mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
                return new ResponseEntity(mess, HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/update")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> update(@RequestHeader HttpHeaders headers, @RequestBody Stock stock) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            mStockService.save(stock);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/delete")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mStockService.delete(Integer.parseInt(map.get("stockId").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }
}
