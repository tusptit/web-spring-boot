package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.SizeDao;
import vn.com.kodergang.shop.entity.Size;

import java.util.List;
import java.util.Map;

@Repository("SizeDaoImpl")
public class SizeDaoImpl extends GenericDaoImpl<Size, Integer> implements SizeDao {
    @Override
    public Size findSizeByCode(Map map) {
        String sql = "select * from size where SIZE_CODE=:sizeCode and STATUS='ACTIVE'";
        List<Size> lst = getByCondition(map, sql, Size.class);
        if (lst.size() > 1) {
            return lst.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<Size> getAllSize() {
        String sql = "Select * from Size where STATUS='ACTIVE'";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Size.class));
    }
}
