package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.StockDao;
import vn.com.kodergang.shop.dao.repo.StockRepo;
import vn.com.kodergang.shop.entity.Stock;
import vn.com.kodergang.shop.service.StockService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("StockServiceImpl")
@Transactional
public class StockServiceImpl extends GenericServiceImpl<Stock, Integer> implements StockService {

    @Autowired
    private StockDao dao;

    public StockServiceImpl() {
    }

    @Autowired
    public StockServiceImpl(@Qualifier("StockDaoImpl") GenericDao<Stock, Integer> genericDao) {
        super(genericDao);
        this.dao = (StockDao) genericDao;
    }


    @Override
    public boolean checkExistCode(String code) {
        Map map = new HashMap() {{
            put("stockCode", code);
        }};
        return dao.checkExistCode(map);
    }

    @Override
    public List<Stock> getAllStock() {
        return dao.getAllStock();
    }
}
