package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Size;

import java.util.List;

public interface SizeService extends GenericService<Size, Integer> {
    List<Size> getAllSize();
    boolean findSizeByCode(String sizeCode);
}

