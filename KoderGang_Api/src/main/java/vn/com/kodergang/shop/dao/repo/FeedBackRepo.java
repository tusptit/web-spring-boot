package vn.com.kodergang.shop.dao.repo;

import org.springframework.data.repository.CrudRepository;
import vn.com.kodergang.shop.entity.FeedBack;

public interface FeedBackRepo extends CrudRepository<FeedBack,Integer> {
}
