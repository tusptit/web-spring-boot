package vn.com.kodergang.shop.entity;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import vn.com.kodergang.common.utils.ImageUtil;

import javax.persistence.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ORDER_DETAIL")
public class OrderDetail {
    @Id
    @Column(name = "ORDER_DETAIL_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer orderDetailId;
    @Column(name = "ORDER_ID")
    private Integer orderId;
    @Column(name = "PRODUCT_ID")
    private Integer productId;
    @Column(name = "QUANTITY")
    private Integer quantity;

    @Transient
    private String productName;
    @Transient
    private String productPicture;
    @Transient
    private float productPrice;
    @Transient
    private float productTotal;
    @Transient
    private List<String> productListPicture;

    public float getProductTotal() {
        return productPrice * quantity;
    }

    public void setProductTotal(float productTotal) {
        this.productTotal = productTotal;
    }

    public List<String> getProductListPicture() {
        try {
            if (productPicture != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(productPicture, listType);
                List<String> pictures = new ArrayList<>();
                for (String item : lst) {
                    pictures.add(ImageUtil.readImage("D:/upload/", item));
                }
                return pictures;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productListPicture;
    }

    public void setProductListPicture(List<String> productListPicture) {
        this.productListPicture = productListPicture;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPicture() {
        return ImageUtil.readImage("D:/upload/", productPicture);
    }

    public void setProductPicture(String productPicture) {
        this.productPicture = productPicture;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
