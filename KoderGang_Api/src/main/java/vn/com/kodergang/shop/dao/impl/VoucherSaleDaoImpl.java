package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.VoucherSaleDao;
import vn.com.kodergang.shop.entity.VoucherSale;

import java.util.List;

@Repository("VoucherSaleDaoImpl")
public class VoucherSaleDaoImpl extends GenericDaoImpl<VoucherSale, Integer> implements VoucherSaleDao {
    @Override
    public List<VoucherSale> getAllVoucherSale() {
        String sql = "Select * from VOUCHER_SALE";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(VoucherSale.class));
    }
}
