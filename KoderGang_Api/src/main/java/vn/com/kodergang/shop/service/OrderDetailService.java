package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.OrderDetail;

import java.util.List;
import java.util.Map;

public interface OrderDetailService extends GenericService<OrderDetail, Integer> {
    List<OrderDetail> getOrderDetail(Map map);
}
