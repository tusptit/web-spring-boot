package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.VoucherSale;

import java.util.List;

public interface VoucherSaleService extends GenericService<VoucherSale, Integer> {
    List<VoucherSale> getAllVoucherSale();
}


