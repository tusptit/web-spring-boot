package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.MessagesResponse;
import vn.com.kodergang.shop.entity.VoucherSale;
import vn.com.kodergang.shop.service.impl.VoucherSaleServiceImpl;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.VOUCHER_SALE)
@RestController
public class VoucherSaleController {
    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private VoucherSaleServiceImpl mVoucherSaleService;

    @PostMapping("/getAll")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> getAllVoucherSale(@RequestHeader HttpHeaders headers) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);

        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mVoucherSaleService.getAllVoucherSale());
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/insert")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> insert(@RequestHeader HttpHeaders headers, @RequestBody VoucherSale voucherSale) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {

            mVoucherSaleService.save(voucherSale);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }


    @PostMapping("/update")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> update(@RequestHeader HttpHeaders headers, @RequestBody VoucherSale voucherSale) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {

            mVoucherSaleService.save(voucherSale);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/delete")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mVoucherSaleService.delete(Integer.parseInt(map.get("voucherSaleId").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }
}
