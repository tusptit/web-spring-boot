package vn.com.kodergang.shop.dao.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.shop.entity.Category;

import java.util.List;

@Repository
public interface CategoryRepo extends CrudRepository<Category, Integer> {
//    @Query("select c from Category c where c.categoryName Like %?1%")
//    List<Category> searchByName(String name);
}
