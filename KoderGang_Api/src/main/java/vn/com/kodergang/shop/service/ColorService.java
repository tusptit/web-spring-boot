package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Color;

import java.util.List;
import java.util.Map;

public interface ColorService extends GenericService<Color, Integer> {
    boolean findColorByCode(String colorCode);
    List<Color> getAllColor();
}

