package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Stock;

import java.util.List;
import java.util.Map;

public interface StockDao extends GenericDao<Stock, Integer> {
    List<Stock> getAllStock();

    boolean checkExistCode(Map map);
}

