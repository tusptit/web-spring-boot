package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Blog;

import java.util.List;

public interface BlogService extends GenericService<Blog, Integer> {
    List<Blog> getAllBlog();
}
