package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.AdGroup;

import java.util.List;

public interface AdGroupService extends GenericService<AdGroup, Integer> {
    boolean checkExistCode(String code);

    List<AdGroup> getAllAdGroup();
}
