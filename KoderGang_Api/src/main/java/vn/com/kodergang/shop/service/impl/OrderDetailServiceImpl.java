package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.OrderDao;
import vn.com.kodergang.shop.dao.OrderDetailDao;
import vn.com.kodergang.shop.entity.OrderDetail;
import vn.com.kodergang.shop.service.OrderDetailService;

import java.util.List;
import java.util.Map;

@Service("OrderDetailServiceImpl")
@Transactional
public class OrderDetailServiceImpl extends GenericServiceImpl<OrderDetail, Integer> implements OrderDetailService {


    @Autowired
    private OrderDetailDao dao;

    public OrderDetailServiceImpl() {
    }

    @Autowired
    public OrderDetailServiceImpl(@Qualifier("OrderDetailDaoImpl") GenericDao<OrderDetail, Integer> genericDao) {
        super(genericDao);
        this.dao = (OrderDetailDao) genericDao;
    }

    @Override
    public List<OrderDetail> getOrderDetail(Map map) {
        return dao.getOrderDetail(map);
    }
}
