package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.MessagesResponse;
import vn.com.kodergang.shop.entity.OrderDetail;
import vn.com.kodergang.shop.service.impl.OrderDetailServiceImpl;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.ORDER_DETAIL)
@RestController
public class OrderDetailController {

    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private OrderDetailServiceImpl mOrderService;

    @RequestMapping(value = {"/getOrderDetail"}, method = RequestMethod.POST)
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> getOrderDetail(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mOrderService.getOrderDetail(map));
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/insert")
    @Transactional
    public ResponseEntity save(@RequestBody OrderDetail orderDetail) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            mOrderService.save(orderDetail);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/update")
    @Transactional
    public ResponseEntity update(@RequestBody OrderDetail orderDetail) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            mOrderService.save(orderDetail);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @RequestMapping(value = {"/delete"}, method = RequestMethod.POST)
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mOrderService.delete(Integer.parseInt(map.get("orderDetailId").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }

}
