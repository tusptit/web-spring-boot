package vn.com.kodergang.shop.entity;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import vn.com.kodergang.common.utils.ImageUtil;

import javax.persistence.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PRODUCT")
public class Product {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PRICE")
    private Float price;
    @Column(name = "SALE_PRICE")
    private Float salePrice;
    @Column(name = "DISCOUNT")
    private Integer discount;
    @Column(name = "PICTURE")
    private String picturesString;
    @Column(name = "SHORT_DETAIL")
    private String shortDetail;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "STOCK")
    private String stock;
    @Column(name = "NEW")
    private boolean news;
    @Column(name = "SALE")
    private boolean sale;
    @Column(name = "CATEGORY")
    private String categoryString;
    @Column(name = "COLORS")
    private String colorsString;
    @Column(name = "SIZE")
    private String sizeString;
    @Column(name = "TAGS")
    private String tagsString;
    @Column(name = "RATING")
    private String rating;
    @Column(name = "VARIANT")
    private String variantsString;
    @Column(name = "CATEGORY_ID")
    private Integer categoryId;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "QUANTITY")
    private Integer quantity;

//    @OneToMany(mappedBy="product")
//    private List<SizeRepo> productSize = new ArrayList<>();

    @Transient
    private List<String> size;
    @Transient
    private int ProductID;

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    @Transient
    private Integer Qty;

    public Integer getQty() {
        return Qty;
    }

    public void setQty(Integer qty) {
        Qty = qty;
    }

    @Transient
    private List<String> category;
    @Transient
    private List<String> pictures;
    @Transient
    private List<Image> lstImages;
    @Transient
    private List<String> colors;
    @Transient
    private List<String> tags;
    @Transient
    private List<String> variant;
    @Transient
    private String categoryName;
    @Transient
    private List<String> listImage;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<Image> getLstImages() {
        return lstImages;
    }

    public void setLstImages(List<Image> lstImages) {
        this.lstImages = lstImages;
    }

    public List<String> getListImage() {
        return listImage;
    }

    public void setListImage(List<String> listImage) {
        this.listImage = listImage;
    }

    public List<String> getSize() {
        try {
            if (sizeString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(sizeString, listType);
                return lst;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return size;
    }

    public List<String> getCategory() {
        try {
            if (categoryString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(categoryString, listType);
                return lst;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return category;
    }

    public List<String> getPictures() {
        try {
            if (picturesString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(picturesString, listType);
                List<String> pictures = new ArrayList<>();
                for (String item : lst) {
                    pictures.add(ImageUtil.readThumbnailImage("D:/upload/", item));
                }
                return pictures;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return pictures;
    }

    public List<String> getColors() {
        try {
            if (colorsString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(colorsString, listType);
                return lst;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return colors;
    }

    public List<String> getTags() {
        try {
            if (tagsString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(tagsString, listType);
                return lst;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return tags;
    }

    public List<String> getVariant() {
        try {
            if (variantsString != null) {
                Type listType = new TypeToken<List<String>>() {
                }.getType();
                Gson gson = new Gson();
                List<String> lst = gson.fromJson(variantsString, listType);
                return lst;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return variant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Float salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getPicturesString() {
        return picturesString;
    }

    public void setPicturesString(String picturesString) {
        this.picturesString = picturesString;
    }

    public String getShortDetail() {
        return shortDetail;
    }

    public void setShortDetail(String shortDetail) {
        this.shortDetail = shortDetail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public boolean isNews() {
        return news;
    }

    public void setNews(boolean news) {
        this.news = news;
    }

    public boolean isSale() {
        return sale;
    }

    public void setSale(boolean sale) {
        this.sale = sale;
    }

    public String getCategoryString() {
        return categoryString;
    }

    public void setCategoryString(String categoryString) {
        this.categoryString = categoryString;
    }

    public String getColorsString() {
        return colorsString;
    }

    public void setColorsString(String colorsString) {
        this.colorsString = colorsString;
    }

    public String getSizeString() {
        return sizeString;
    }

    public void setSizeString(String sizeString) {
        this.sizeString = sizeString;
    }

    public String getTagsString() {
        return tagsString;
    }

    public void setTagsString(String tagsString) {
        this.tagsString = tagsString;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVariantsString() {
        return variantsString;
    }

    public void setVariantsString(String variantsString) {
        this.variantsString = variantsString;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSize(List<String> size) {
        this.size = size;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setVariant(List<String> variant) {
        this.variant = variant;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
