package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.StockDao;
import vn.com.kodergang.shop.entity.Stock;

import java.util.List;
import java.util.Map;

@Repository("StockDaoImpl")
public class StockDaoImpl extends GenericDaoImpl<Stock, Integer> implements StockDao {
    @Override
    public List<Stock> getAllStock() {
        String sql = "Select * from Stock";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Stock.class));
    }

    @Override
    public boolean checkExistCode(Map map) {
        String sql = "Select * from stock where stock_Code=:stockCode";
        List<Stock> list = getByCondition(map, sql, Stock.class);
        if (list.size() > 0) {
            return true;
        }
        return false;
    }
}
