package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.AdGroupDao;
import vn.com.kodergang.shop.dao.StockDao;
import vn.com.kodergang.shop.entity.AdGroup;
import vn.com.kodergang.shop.entity.Stock;

import java.util.List;
import java.util.Map;

@Repository("AdGroupDaoImpl")
public class AdGroupDaoImpl extends GenericDaoImpl<AdGroup, Integer> implements AdGroupDao {

    @Override
    public List<AdGroup> getAllAdGroup() {
        String sql = "Select * from AD_GROUP";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(AdGroup.class));
    }

    @Override
    public boolean checkExistCode(Map map) {
        String sql = "Select * from AD_GROUP where GROUP_CODE=:groupCode";
        List<AdGroup> list = getByCondition(map, sql, AdGroup.class);
        if (list.size() > 0) {
            return true;
        }
        return false;
    }
}
