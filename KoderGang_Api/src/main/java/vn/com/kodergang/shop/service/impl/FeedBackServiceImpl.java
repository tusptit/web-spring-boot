package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.FeedBackDao;
import vn.com.kodergang.shop.entity.FeedBack;
import vn.com.kodergang.shop.service.FeedBackService;

import java.util.List;
import java.util.Map;


@Service("FeedBackServiceImpl")
@Transactional
public class FeedBackServiceImpl extends GenericServiceImpl<FeedBack, Integer> implements FeedBackService {
    @Autowired
    private FeedBackDao dao;

    public FeedBackServiceImpl() {
    }

    @Autowired
    public FeedBackServiceImpl(@Qualifier("FeedBackDaoImpl") GenericDao<FeedBack, Integer> genericDao) {
        super(genericDao);
        this.dao = (FeedBackDao) genericDao;
    }

    @Override
    public List<FeedBack> getFeedBackByProductId(Map map) {
        return dao.getFeedBackByProductId(map);
    }
}
