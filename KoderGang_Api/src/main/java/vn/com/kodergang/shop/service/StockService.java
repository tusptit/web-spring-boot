package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Stock;

import java.util.List;

public interface StockService extends GenericService<Stock, Integer> {
    boolean checkExistCode(String code);

    List<Stock> getAllStock();
}
