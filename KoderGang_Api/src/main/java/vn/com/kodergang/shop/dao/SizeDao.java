package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Size;

import java.util.List;
import java.util.Map;

public interface SizeDao extends GenericDao<Size, Integer> {
    Size findSizeByCode(Map map);
    List<Size> getAllSize();
}