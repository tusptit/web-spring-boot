package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Blog;

import java.util.List;

public interface BlogDao extends GenericDao<Blog, Integer> {
    List<Blog> getAllBlog();
}
