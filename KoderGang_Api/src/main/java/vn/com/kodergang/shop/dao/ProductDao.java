package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Product;

import java.util.List;
import java.util.Map;

public interface ProductDao extends GenericDao<Product,Integer> {
    List<Product> getAllProduct();
}
