package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Category;

import java.util.List;
import java.util.Map;

public interface CategoryDao extends GenericDao<Category,Integer> {
    List<Category> getAllCategory();
    boolean checkExistCode(Map map);
}
