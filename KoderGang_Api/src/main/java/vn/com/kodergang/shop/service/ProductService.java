package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Product;

import java.util.List;
import java.util.Map;

public interface ProductService extends GenericService<Product, Integer> {
    List<Product> getAllProduct();
}
