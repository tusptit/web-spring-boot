package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.common.utils.DateUtil;
import vn.com.kodergang.common.utils.StringUtil;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.*;
import vn.com.kodergang.shop.service.ProductService;
import vn.com.kodergang.shop.service.impl.EmailService;
import vn.com.kodergang.shop.service.impl.OrderDetailServiceImpl;
import vn.com.kodergang.shop.service.impl.OrderServiceImpl;

import java.util.*;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.ORDER)
@RestController
public class OrderController {
    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private OrderServiceImpl mOrderService;
    @Autowired
    private OrderDetailServiceImpl mOrderDetailService;
    @Autowired
    private EmailService emailService;

    @Autowired
    private ProductService productService;


    @PostMapping("/getAll")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> getAllOrder(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);

        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mOrderService.getAllOrder(map));
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    public void getSendMail(String email, String ordercode, String name, float total, List<ProductCart> products) {
        Mail mail = new Mail();
        mail.setFrom("leduclong100@gmail.com");
        mail.setTo(email);
        mail.setSubject("ĐƠN HÀNG CỦA BẠN TỪ KODER SHOP");
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("customerName", name);
        model.put("total", total);
        model.put("products", products);
        model.put("orderCode", ordercode);
        model.put("location", "21 KeangNam, Phạm Hùng Hà Nội");
        model.put("phone", "0941958897");
        mail.setModel(model);
        MessagesResponse mess = new MessagesResponse();
        try {
            emailService.sendSimpleMessage(mail);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }

    @PostMapping("/insert")
    @Transactional
    public ResponseEntity insert(@RequestBody Map map) {
        Order order = new Order();
        order.setEmail(map.get("email").toString());
        order.setUserName(map.get("userName").toString());
        order.setAddress(map.get("address").toString());
        order.setPhone(map.get("phone").toString());
        order.setStatus(map.get("status").toString());
        order.setTotal(Float.parseFloat(map.get("total").toString()));
        order.setOrderCode(StringUtil.generateCode(8));
        order.setCreateTime(new Date());

        order.setCreateTimeString(DateUtil.dateToString(new Date(), DateUtil.FORMAT_DATE_TIME2));
        List<Object> list = (List<Object>) map.get("listProduct");
        List<ProductCart> products = new ArrayList<>();
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            int orderId = mOrderService.save(order).getOrderId();
            for (Object object : list) {
                OrderDetail orderDetail = new OrderDetail();
                Map<String, Object> productCartMap = (Map<String, Object>) object;
                Product product = productService.findOne(Integer.parseInt((productCartMap.get("ProductID").toString())));
                orderDetail.setOrderId(orderId);
                products.add(new ProductCart(productCartMap.get("ProductID").toString(), order.getOrderCode()
                        , productCartMap.get("ProductName").toString(), productCartMap.get("Qty").toString(), String.valueOf(product.getPrice())));
                orderDetail.setProductId(Integer.parseInt(productCartMap.get("ProductID").toString()));
                orderDetail.setQuantity(Integer.parseInt(productCartMap.get("Qty").toString()));
                mOrderDetailService.save(orderDetail);
                product.setQuantity(product.getQuantity() - Integer.parseInt(productCartMap.get("Qty").toString()));
                productService.save(product);
            }
            getSendMail(order.getEmail(), order.getOrderCode(), order.getUserName(), order.getTotal(), products);
            mess.setData(order);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/update")
    @Transactional
    public ResponseEntity update(@RequestBody Order order) {
        OrderDetail orderDetail = new OrderDetail();
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            mOrderService.save(order);
            orderDetail.setOrderId(order.getOrderId());
            mOrderDetailService.save(orderDetail);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/delete")
    @Transactional
    public ResponseEntity delete(@RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mOrderService.delete(Integer.parseInt(map.get("OrderId").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }


}
