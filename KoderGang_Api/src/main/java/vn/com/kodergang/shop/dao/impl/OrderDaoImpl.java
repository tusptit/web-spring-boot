package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.common.utils.StringUtil;
import vn.com.kodergang.shop.dao.OrderDao;
import vn.com.kodergang.shop.entity.Order;

import java.util.List;
import java.util.Map;

@Repository("OrderDaoImpl")
public class OrderDaoImpl extends GenericDaoImpl<Order, Integer> implements OrderDao {
    @Override
    public List<Order> getAllOrder(Map map) {
        String sql = "Select Orders.order_id orderId " +
                ",user_name userName " +
                ",address address " +
                ",phone phone " +
                ",status status " +
                ",total total" +
                ",create_time createTime " +
                ",order_detail_id orderDetailId " +
                ",product_id productId " +
                ",quantity quantity " +
                ",email email " +
                "from " +
                "Orders join order_detail on orders.order_id=order_detail.order_id";
        return getByCondition(map, sql, Order.class);
    }


}
