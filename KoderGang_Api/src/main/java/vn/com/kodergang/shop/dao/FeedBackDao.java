package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.FeedBack;

import java.util.List;
import java.util.Map;

public interface FeedBackDao extends GenericDao<FeedBack,Integer> {
    List<FeedBack> getFeedBackByProductId(Map map);
}
