package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.OrderDao;
import vn.com.kodergang.shop.entity.Order;
import vn.com.kodergang.shop.service.OrderService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("OrderServiceImpl")
@Transactional
public class OrderServiceImpl extends GenericServiceImpl<Order, Integer> implements OrderService {

    @Autowired
    private OrderDao dao;

    public OrderServiceImpl() {
    }

    @Autowired
    public OrderServiceImpl(@Qualifier("OrderDaoImpl") GenericDao<Order, Integer> genericDao) {
        super(genericDao);
        this.dao = (OrderDao) genericDao;
    }


    @Override
    public List<Order> getAllOrder(Map map) {
        return dao.getAllOrder(map);
    }
}
