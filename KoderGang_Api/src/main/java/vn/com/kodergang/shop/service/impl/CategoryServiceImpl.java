package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.CategoryDao;
import vn.com.kodergang.shop.dao.repo.CategoryRepo;
import vn.com.kodergang.shop.entity.Category;
import vn.com.kodergang.shop.service.CategoryService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("CategoryServiceImpl")
@Transactional
public class CategoryServiceImpl extends GenericServiceImpl<Category, Integer> implements CategoryService {

    @Autowired
    private CategoryDao dao;

    public CategoryServiceImpl() {
    }

    @Autowired
    public CategoryServiceImpl(@Qualifier("CategoryDaoImpl") GenericDao<Category, Integer> genericDao) {
        super(genericDao);
        this.dao = (CategoryDao) genericDao;
    }

    @Override
    public boolean checkExistCode(String code) {
        Map map = new HashMap() {{
            put("categoryCode", code);
        }};
        return dao.checkExistCode(map);
    }

    @Override
    public List<Category> getAllCategory() {
        return dao.getAllCategory();
    }
}