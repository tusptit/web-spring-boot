package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Category;

import java.util.List;

public interface CategoryService extends GenericService<Category, Integer> {
    boolean checkExistCode(String code);

    List<Category> getAllCategory();
}
