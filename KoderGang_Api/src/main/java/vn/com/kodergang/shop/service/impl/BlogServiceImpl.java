package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.BlogDao;
import vn.com.kodergang.shop.entity.Blog;
import vn.com.kodergang.shop.service.BlogService;

import java.util.List;

@Service("BlogServiceImpl")
@Transactional
public class BlogServiceImpl extends GenericServiceImpl<Blog, Integer> implements BlogService {

    @Autowired
    private BlogDao dao;

    public BlogServiceImpl() {
    }

    @Autowired
    public BlogServiceImpl(@Qualifier("BlogDaoImpl") GenericDao<Blog, Integer> genericDao) {
        super(genericDao);
        this.dao = (BlogDao) genericDao;
    }

    @Override
    public List<Blog> getAllBlog() {
        return dao.getAllBlog();
    }
}
