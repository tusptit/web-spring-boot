package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.OrderDetailDao;
import vn.com.kodergang.shop.entity.OrderDetail;

import java.util.List;
import java.util.Map;

@Repository("OrderDetailDaoImpl")
public class OrderDetailDaoImpl extends GenericDaoImpl<OrderDetail, Integer> implements OrderDetailDao {

    @Override
    public List<OrderDetail> getOrderDetail(Map map) {
        String sql = "SELECT ORDER_DETAIL_ID orderDetailId,\n" +
                "ORDER_ID orderId,\n" +
                "PRODUCT_ID productId,\n" +
                "order_detail.QUANTITY quantity,\n" +
                "product.NAME productName,\n" +
                "product.PICTURE productPicture,\n" +
                "product.price productPrice\n" +
                "FROM order_detail\n" +
                "join product on product.id=order_detail.product_Id where ORDER_ID=:orderId";
        return getByCondition(map, sql, OrderDetail.class);
    }
}
