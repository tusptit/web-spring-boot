package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.AdGroup;

import java.util.List;
import java.util.Map;

public interface AdGroupDao extends GenericDao<AdGroup, Integer> {
    List<AdGroup> getAllAdGroup();

    boolean checkExistCode(Map map);
}
