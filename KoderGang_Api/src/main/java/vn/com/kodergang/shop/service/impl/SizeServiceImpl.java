package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.SizeDao;
import vn.com.kodergang.shop.entity.Size;
import vn.com.kodergang.shop.service.SizeService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("SizeServiceImpl")
@Transactional
public class SizeServiceImpl extends GenericServiceImpl<Size, Integer> implements SizeService {
    @Autowired
    private SizeDao dao;

    public SizeServiceImpl() {
    }

    @Autowired
    public SizeServiceImpl(@Qualifier("SizeDaoImpl") GenericDao<Size, Integer> genericDao) {
        super(genericDao);
        this.dao = (SizeDao) genericDao;
    }

    @Override
    public List<Size> getAllSize() {
        return dao.getAllSize();
    }

    @Override
    public boolean findSizeByCode(String sizeCode) {
        Map map = new HashMap() {{
            put("sizeCode", sizeCode);
        }};
        return dao.findSizeByCode(map) != null;
    }
}
