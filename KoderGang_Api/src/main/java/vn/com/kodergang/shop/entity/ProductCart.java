package vn.com.kodergang.shop.entity;


public class ProductCart {
    private String ProductID;
    private String OrderCode;
    private String ProductImage;
    private String ProductName;
    private String ProductPrice;
    private String Qty;
    private String Rate;
    private String StockStatus;

    public ProductCart(String productID, String orderCode, String productName, String productPrice, String qty) {
        ProductID = productID;
        OrderCode = orderCode;
        ProductName = productName;
        ProductPrice = productPrice;
        Qty = qty;
    }

    public String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(String orderCode) {
        OrderCode = orderCode;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getStockStatus() {
        return StockStatus;
    }

    public void setStockStatus(String stockStatus) {
        StockStatus = stockStatus;
    }
}
