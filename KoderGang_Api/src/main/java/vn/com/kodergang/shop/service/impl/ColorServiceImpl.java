package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.ColorDao;
import vn.com.kodergang.shop.entity.Color;
import vn.com.kodergang.shop.service.ColorService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("ColorServiceImpl")
@Transactional
public class ColorServiceImpl extends GenericServiceImpl<Color, Integer> implements ColorService {
    @Autowired
    private ColorDao dao;

    public ColorServiceImpl() {
    }

    @Autowired
    public ColorServiceImpl(@Qualifier("ColorDaoImpl") GenericDao<Color, Integer> genericDao) {
        super(genericDao);
        this.dao = (ColorDao) genericDao;
    }

    @Override
    public boolean findColorByCode(String colorCode) {
        Map map = new HashMap() {{
            put("colorCode", colorCode);
        }};
        return dao.findColorByCode(map) != null;
    }

    @Override
    public List<Color> getAllColor() {
        return dao.getAllColor();
    }
}
