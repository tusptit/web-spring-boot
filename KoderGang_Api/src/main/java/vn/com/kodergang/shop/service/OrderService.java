package vn.com.kodergang.shop.service;

import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.shop.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderService extends GenericService<Order, Integer> {

    List<Order> getAllOrder(Map map);

}
