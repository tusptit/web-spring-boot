package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao extends GenericDao<Order, Integer> {
    List<Order> getAllOrder(Map map);
}
