package vn.com.kodergang.shop.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FEEDBACK")
public class FeedBack {
    @Id
    @Column(name = "IDFEEDBACK", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer feedBackId;
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "CUSTOMER_EMAIL")
    private String customerEmail;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "PRODUCT_ID")
    private String productId;
    @Column(name = "CREATE_TIME")
    private Date createTime;

    @Transient
    private String timeFeedBack;

    public String getTimeFeedBack() {
        return timeFeedBack;
    }

    public void setTimeFeedBack(String timeFeedBack) {
        this.timeFeedBack = timeFeedBack;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getFeedBackId() {
        return feedBackId;
    }

    public void setFeedBackId(Integer feedBackId) {
        this.feedBackId = feedBackId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
