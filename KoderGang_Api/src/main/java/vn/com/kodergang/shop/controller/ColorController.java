package vn.com.kodergang.shop.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import vn.com.kodergang.shop.Constant;
import vn.com.kodergang.shop.entity.Color;
import vn.com.kodergang.shop.entity.MessagesResponse;
import vn.com.kodergang.shop.service.impl.ColorServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RequestMapping("/" + Constant.API_PATH.COLOR)
@RestController
public class ColorController {
    Logger logger = LogManager.getLogger(getClass());
    @Autowired
    private ColorServiceImpl mColorService;

    @PostMapping("/getAll")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> getAllColor(@RequestHeader HttpHeaders headers) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);

        MessagesResponse mess = new MessagesResponse();
        try {
            mess.setData(mColorService.getAllColor());
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }
    }

    @PostMapping("/insert")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> insert(@RequestHeader HttpHeaders headers, @RequestBody Color color) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            if (mColorService.findColorByCode(color.getColorCode())) {
                mess.setCode(Constant.RESPONSE_CODE.NOT_EXISTS);
                mess.setStatus(Constant.RESPONSE_STATUS.ERROR);
                mess.setMessage("Màu đã tồn tại vui lòng chọn màu khác");
                return new ResponseEntity(mess, HttpStatus.OK);
            }
            color.setCreateTime(new Date());
            mColorService.save(color);
            mess.setStatus(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (
                Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/update")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> update(@RequestHeader HttpHeaders headers, @RequestBody Color color) {
        logger.info(" user: "
                + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.BEGIN);
        MessagesResponse mess = new MessagesResponse();
        try {
            color.setCreateTime(new Date());
            mColorService.save(color);
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (
                Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }


    }

    @PostMapping("/delete")
    @Transactional
    @PreAuthorize("apiSecured(#headers, '" + Constant.PERMISSION.ADMIN + "', '" + Constant.PRIVILEGE.ADMIN + "')")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestHeader HttpHeaders headers, @RequestBody Map map) {
        MessagesResponse mess = new MessagesResponse();
        try {
            mColorService.delete(Integer.parseInt(map.get("id").toString()));
            mess.setMessage(Constant.RESPONSE_STATUS.SUCCESS);
            return new ResponseEntity(mess, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity(mess.error(e), HttpStatus.OK);
        } finally {
            logger.info(" user: "
                    + SecurityContextHolder.getContext().getAuthentication().getName() + Constant.LOG.END);
        }

    }

}
