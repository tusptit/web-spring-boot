package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.VoucherSale;

import java.util.List;

public interface VoucherSaleDao extends GenericDao<VoucherSale, Integer> {
    List<VoucherSale> getAllVoucherSale();
}