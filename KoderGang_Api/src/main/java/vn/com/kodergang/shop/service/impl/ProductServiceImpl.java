package vn.com.kodergang.shop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.common.generics.GenericService;
import vn.com.kodergang.common.generics.impl.GenericServiceImpl;
import vn.com.kodergang.shop.dao.AdUserDao;
import vn.com.kodergang.shop.dao.ProductDao;
import vn.com.kodergang.shop.dao.repo.ProductRepo;
import vn.com.kodergang.shop.entity.AdUser;
import vn.com.kodergang.shop.entity.Product;
import vn.com.kodergang.shop.service.AdUserService;
import vn.com.kodergang.shop.service.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("ProductServiceImpl")
@Transactional
public class ProductServiceImpl extends GenericServiceImpl<Product, Integer> implements ProductService {

    @Autowired
    private ProductDao dao;

    public ProductServiceImpl() {
    }

    @Autowired
    public ProductServiceImpl(@Qualifier("ProductDaoImpl") GenericDao<Product, Integer> genericDao) {
        super(genericDao);
        this.dao = (ProductDao) genericDao;
    }


    @Override
    public List<Product> getAllProduct() {
        return dao.getAllProduct();
    }
}
