package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.ColorDao;
import vn.com.kodergang.shop.entity.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("ColorDaoImpl")
public class ColorDaoImpl extends GenericDaoImpl<Color, Integer> implements ColorDao {
    @Override
    public List<Color> getAllColor() {
        String sql = "Select * from Colors where STATUS='ACTIVE'";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Color.class));

    }

    @Override
    public Color findColorByCode(Map map) {
        String sql = "select * from Colors where COLOR_CODE=:colorCode and STATUS='ACTIVE'";
        List<Color> lst = getByCondition(map, sql, Color.class);
        if (lst.size() > 1) {
            return lst.get(0);
        } else {
            return null;
        }
    }
}
