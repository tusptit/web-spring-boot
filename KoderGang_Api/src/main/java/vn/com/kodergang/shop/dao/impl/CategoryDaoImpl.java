package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.CategoryDao;
import vn.com.kodergang.shop.entity.Category;

import java.util.List;
import java.util.Map;

@Repository("CategoryDaoImpl")
public class CategoryDaoImpl extends GenericDaoImpl<Category, Integer> implements CategoryDao {

    @Override
    public List<Category> getAllCategory() {
        // map.put("productName", StringUtil.getParamSQLWithLike(map.get("productName").toString()));
        String sql = "Select * from Category WHERE STATUS='ACTIVE'";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Category.class));
    }

    @Override
    public boolean checkExistCode(Map map) {
        String sql = "Select * from Category where category_Code =:categoryCode";
        List<Category> list = getByCondition(map, sql, Category.class);
        if (list.size() > 0) {
            return true;
        }
        return false;
    }
}
