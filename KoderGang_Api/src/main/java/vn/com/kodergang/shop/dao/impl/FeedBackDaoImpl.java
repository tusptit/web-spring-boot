package vn.com.kodergang.shop.dao.impl;

import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.shop.dao.FeedBackDao;
import vn.com.kodergang.shop.entity.FeedBack;

import java.util.List;
import java.util.Map;


@Repository("FeedBackDaoImpl")
public class FeedBackDaoImpl extends GenericDaoImpl<FeedBack, Integer> implements FeedBackDao {

    @Override
    public List<FeedBack> getFeedBackByProductId(Map map) {
        String sql = "SELECT customer_Name customerName,CONTENT content, DATE_FORMAT(CREATE_TIME,'%m-%d-%Y') timeFeedBack from feedBack where product_Id=:productId";
        return getByCondition(map, sql, FeedBack.class);
    }
}
