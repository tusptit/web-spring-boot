package vn.com.kodergang.shop.dao;

import vn.com.kodergang.common.generics.GenericDao;
import vn.com.kodergang.shop.entity.Color;

import java.util.List;
import java.util.Map;

public interface ColorDao extends GenericDao<Color, Integer> {
    List<Color> getAllColor();
    Color findColorByCode(Map map);
}
