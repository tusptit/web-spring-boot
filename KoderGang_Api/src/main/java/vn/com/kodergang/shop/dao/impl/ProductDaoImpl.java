package vn.com.kodergang.shop.dao.impl;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import vn.com.kodergang.common.generics.impl.GenericDaoImpl;
import vn.com.kodergang.common.utils.StringUtil;
import vn.com.kodergang.shop.dao.ProductDao;
import vn.com.kodergang.shop.entity.Product;

import java.util.List;
import java.util.Map;

@Repository("ProductDaoImpl")
public class ProductDaoImpl extends GenericDaoImpl<Product, Integer> implements ProductDao {
    @Override
    public List<Product> getAllProduct() {
        String sql = "select ID id" +
                ",NAME name" +
                ",price price" +
                ",sale_price salePrice " +
                ",DISCOUNT discount" +
                ",PICTURE picturesString" +
                ",SHORT_DETAIL shortDetail" +
                ",DESCRIPTION description" +
                ",STOCK stock" +
                ",NEW news" +
                ",SALE sale" +
                ",CATEGORY categoryString " +
                ",COLORS colorsString" +
                ",SIZE sizeString" +
                ",TAGS tagsString" +
                ",RATING rating" +
                ",VARIANT variantsString" +
                ",CATEGORY_ID categoryId" +
                ",STATUS status" +
                ",product.QUANTITY quantity" +
                " from product";
        return namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Product.class));
    }


}
