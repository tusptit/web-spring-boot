export default [
    {
        "menu_title": "Trang Chủ",
        "type": "menu",
        "path": "/",
        "icon": "FaHome"
    },
    {
        "menu_title": "Shop",
        "path": "/shop",
        "type": "subMenu",
        //"child_routes": [],
    },
    {
        "menu_title": "Về Chúng Tôi",
        "path": "/Aboutus",
        "mega": true,
        "icon": "party_mode",
        "type": "subMenu",
        //"child_routes": []
    },   {
        "menu_title": "Liên Hệ",
        "path": "/Contactus",
        "mega": true,
        "icon": "party_mode",
        "type": "subMenu",
        //"child_routes": []
    },
]
