import BaseService from "./BaseService";
import RestAPIHelper from "../common/RestAPIHelper";
import Constants from "../common/Constants";

export default class FeedBackService extends BaseService {

    getAll = (data, _callback) => {
        RestAPIHelper.post(Constants.API_URL + "product/getAllFeedBack", data, _callback);
    };
    insert = (data, _callback) => {
        RestAPIHelper.post(Constants.API_URL + "product/insertFeedBack", data, _callback);
    };

}
