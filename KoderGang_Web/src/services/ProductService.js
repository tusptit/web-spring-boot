import BaseService from "./BaseService";
import RestAPIHelper from "../common/RestAPIHelper";
import Constants from "../common/Constants";

export default class ProductService extends BaseService {

    getAll = (data, _callback) => {
        RestAPIHelper.get(Constants.API_URL + "product/getAll", _callback);
    };
    getOne = (data, _callback) => {
        RestAPIHelper.postNotAuthorization(Constants.API_URL + "product/getOne", _callback, data);
    };
    insert = (data, _callback) => {
        RestAPIHelper.post(Constants.API_URL + "product/insert", data, _callback);
    };

    update = (data, _callback) => {
        RestAPIHelper.post(Constants.API_URL + "product/update", data, _callback);
    };

    delete = (data, _callback) => {
        console.log(data);
        RestAPIHelper.post(Constants.API_URL + "product/delete", data, _callback);
    };
}
