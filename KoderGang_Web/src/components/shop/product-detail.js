/**
 *  Shop Product Detail Page
 */
import React, {Component} from 'react';
import ProductSlider from '../../widgets/ProductSlider';
import {Link} from 'react-router-dom';
import PostDetail from './PostDetail';
import {Button, Container, FormGroup, Nav, NavItem, NavLink, Row, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import {connect} from 'react-redux';
import {getFilterProductsdata} from "../../services";
import ProductService from "../../services/ProductService";
import Loader from "react-loader-spinner";
import {onChangeValue} from "../../common/Util";
import FeedBackService from "../../services/FeedBackService";
import {showMessage} from "../modal/Modal";

const relatedslider = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 575,
            settings: {
                slidesToShow: 1
            }
        }
    ]
};

class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: '1',
            data: {
                customerName: '',
                customerEmail: '',
                content: '',
                productId: parseInt(this.props.match.params.id),
            },
            feedBack: [],
            errors: {}
        };
        this.toggle = this.toggle.bind(this);
        this.service = new ProductService();
        this.serviceFeedBack = new FeedBackService();
    }

    UNSAFE_componentWillMount = async () => {
        this.getFeedBack();
        this.getProduct();

    };

    getProduct = async () => {
        await this.service.getOne({
            id: parseInt(this.props.match.params.id)
        }, async result => {
            this.setState({
                product: result.data
            })
        });
    };
    getFeedBack = async () => {
        await this.serviceFeedBack.getAll({
            productId: parseInt(this.props.match.params.id)
        }, async result => {
            this.setState({
                feedBack: result.data
            })
        });
    };

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    onChangeText = (name, e) => {
        onChangeValue(this, name, e)
    };
    validate = () => {
        let data = this.state.data;
        let isValid = true;
        let errors = [];
        if (!data["customerName"]) {
            isValid = false;
            errors["customerName"] = "Bạn phải nhập tên"
        }
        if (!data["customerEmail"]) {
            isValid = false;
            errors["customerEmail"] = "Bạn phải nhập email"
        }
        if (data["customerEmail"] && !/.+@.+\.[A-Za-z]+$/.test(data["customerEmail"])) {
            isValid = false;
            errors["customerEmail"] = "Email phải đúng định dạng : example@gmail.com"
        }
        if (!data["content"]) {
            isValid = false;
            errors["content"] = "Bạn phải nhập đánh giá"
        }
        this.setState({
            errors: errors
        });
        return isValid;
    };
    _onSubmitFeedBack = async () => {
        if (this.validate()) {
            this.serviceFeedBack.insert(this.state.data, async () => {
                showMessage("Cảm ơn bạn đã đánh giá sản phẩm");
                this.getFeedBack();
                await this.setState({
                    data: {
                        customerName: '',
                        customerEmail: '',
                        content: '',
                        productId: parseInt(this.props.match.params.id),
                    }
                })
            })
        }
    };

    render() {
        const product = this.state.product;
        return (
            <div>
                {product ?
                    <div className="site-content">
                        <div className="inner-intro">
                            <Container>
                                <Row className="intro-title align-items-center">
                                    <div className="col-12">
                                        <ul className="ciyashop_breadcrumbs page-breadcrumb breadcrumbs">
                                            <li className="home">
                                    <span className="item-element">
                                    <Link className="bread-link bread-home" to="/">Trang chủ</Link>
                                    </span>
                                            </li>
                                            <li><span className="item-element">{product.category}</span></li>
                                            <li><span className="item-element">{product.name}</span></li>
                                        </ul>
                                    </div>
                                </Row>
                            </Container>
                        </div>
                        <div className="content-wrapper section-ptb">
                            <Container>
                                <PostDetail product={product} tabid={this.state.activeTab}/>
                                <div className="product-content-bottom">

                                    <Nav tabs>
                                        <NavItem active>
                                            <NavLink className={classnames({active: this.state.activeTab === '1'})}
                                                     onClick={() => {
                                                         this.toggle('1');
                                                     }}>Mô tả</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink className={classnames({active: this.state.activeTab === '2'})}
                                                     onClick={() => {
                                                         this.toggle('2');
                                                     }}>Nhận xét</NavLink>
                                        </NavItem>
                                        <NavItem disabled>
                                            <NavLink className={classnames({active: this.state.activeTab === '3'})}
                                                     onClick={() => {
                                                         this.toggle('3');
                                                     }}>Tùy chỉnh</NavLink>
                                        </NavItem>
                                    </Nav>
                                    <TabContent activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            <div className="tab-content" id="myTabContent">
                                                <div className="tab-pane fade show active" id="description"
                                                     role="tabpanel" aria-labelledby="home-tab">
                                                    {product.description}
                                                </div>
                                            </div>
                                        </TabPane>
                                        <TabPane tabId="2">
                                            <div className="product-reviews">
                                                <div>
                                                    {
                                                        this.state.feedBack &&
                                                        this.state.feedBack.map(item =>
                                                            <div style={{
                                                                border: "1px solid #dee2e6", padding: "1em",
                                                                margin: "0 0 1.618em"
                                                            }}>
                                                                <p style={{
                                                                    fontSize: '16px',
                                                                    color: 'black'
                                                                }}>{item.customerName} <i style={{color: '#FFBE00'}}
                                                                                          className="fa fa-star"/>
                                                                    <p style={{fontSize: '12px'}}>{item.timeFeedBack}</p>
                                                                </p>
                                                                <p style={{fontSize: '14px'}}>{item.content}</p>
                                                            </div>)
                                                    }
                                                </div>
                                                <h6>Thêm một bài đánh giá</h6>
                                                <p>Địa chỉ email của bạn sẽ không được công bố. Các trường bắt buộc được
                                                    đánh dấu *</p>
                                                <form>
                                                    <div class="form-group">
                                                        <label>Tên</label>
                                                        <input onChange={this.onChangeText.bind(this, "customerName")}
                                                               type="Text" class="form-control"/>
                                                        <span hidden={!this.state.errors['customerName']}
                                                              className={'errors'}>{this.state.errors['customerName']}</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input onChange={this.onChangeText.bind(this, "customerEmail")}
                                                               type="Text" class="form-control"/>
                                                        <span hidden={!this.state.errors['customerEmail']}
                                                              className={'errors'}>{this.state.errors['customerEmail']}</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label className="mb-0">Đánh giá *</label>
                                                        <textarea onChange={this.onChangeText.bind(this, "content")}
                                                                  className="form-control" rows="3"/>
                                                        <span hidden={!this.state.errors['content']}
                                                              className={'errors'}>{this.state.errors['content']}</span>
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input"
                                                               id="exampleCheck1"/>
                                                        <label class="form-check-label" for="exampleCheck1">Lưu tên,
                                                            email và trang web của tôi trong trình duyệt này cho lần
                                                            tiếp theo tôi nhận xét.</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <Button onClick={this._onSubmitFeedBack}
                                                                class="btn btn-primary">Cập nhật</Button>
                                                    </div>
                                                </form>
                                            </div>
                                        </TabPane>
                                        <TabPane tabId="3">
                                            <p>
                                                {product.shortDetail}
                                            </p>
                                        </TabPane>
                                    </TabContent>
                                    <div className="related products">
                                        <h2>Sản phẩm tưởng tự</h2>
                                        <div className="row">
                                            <ProductSlider product={this.props.products} settings={relatedslider}/>
                                        </div>
                                    </div>
                                </div>
                            </Container>
                        </div>
                    </div>
                    :
                    null
                }
            </div>


        )
    }
}

const mapDispatchToProps = (state) => ({
    products: getFilterProductsdata(state.data, state.filters)
});
export default connect(
    mapDispatchToProps, {}
)(ProductDetail)
