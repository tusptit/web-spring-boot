/*
* Admin Site Product Edit Page
*/
import React, {Component} from 'react';
import Slider from "react-slick";
import {Link} from 'react-router-dom';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

import {FormGroup, Input, Label, Row} from 'reactstrap';
import ImageUploader from 'react-images-upload';
import ProductService from "../services/ProductService";
import {onChangeSelect, onChangeValue} from "../common/Util";
import {showMessage} from "../components/modal/Modal";
import ColorService from "../services/ColorService";
import CategoryService from "../services/CategoryService";
import SizeService from "../services/SizeService";

const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
};
const productslider = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1
};

class ProductEditDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pictures: this.props.product.pictures,
            picturesString: JSON.parse(this.props.product.picturesString),
            lstImage: [],
            photoIndex: 0,
            isOpen: false,
            errors: [],
            ErrorMsg: '',
            data: {
                id: this.props.product.id,
                name: this.props.product.name,
                shortDetails: this.props.product.shortDetails,
                price: this.props.product.price,
                salePrice: this.props.product.price,
                description: this.props.product.description,
                stock: 1,
                new: true,
                quantity: this.props.product.quantity,
                tagsString: [],
                variantsString: [],
                rating: 4,
                sizeString: this.props.product.size,
                colorsString: this.props.product.colors,
                categoryString: this.props.product.tags
            },
        };
        this.serviceColor = new ColorService();
        this.serviceCategory = new CategoryService();
        this.serviceSize = new SizeService();
        this.serviceProduct = new ProductService();
    }

    componentWillMount = async () => {
        await this.getCategory();
        await this.getColor();
        await this.getSize();
    };

    getSize = async () => {
        this.serviceSize.getAll({}, result => {
            this.setState({
                size: result.data
            })
        });

    };
    getColor = async () => {
        await this.serviceColor.getAll({}, result => {
            this.setState({
                colors: result.data
            })
        });

    };
    getCategory = async () => {
        this.serviceCategory.getAll({}, result => {
            this.setState({
                tags: result.data
            })
        });
    };

    validate = () => {
        let data = this.state.data;
        let isValid = true;
        let errors = [];
        if (!data['name']) {
            errors['name'] = 'Tên sản phẩm không được rỗng';
            isValid = false
        }
        if (!data['price']) {
            errors['price'] = 'Giá sản phẩm không được rỗng';
            isValid = false
        }
        if (data['sizeString'].length < 1) {
            errors['sizeString'] = 'Chọn kích cỡ sản phẩm';
            isValid = false
        }
        if (data['colorsString'].length < 1) {
            errors['colorsString'] = 'Chọn màu sắc sản phẩm';
            isValid = false
        }
        if (data['categoryString'].length < 1) {
            errors['categoryString'] = 'Chọn danh mục sản phẩm';
            isValid = false
        }
        if (!data['quantity']) {
            errors['quantity'] = 'Số lượng không được rỗng';
            isValid = false
        }
        if (data['quantity'] && data['quantity'] < 0) {
            errors['quantity'] = 'Số lượng phải lớn hơn 0';
            isValid = false
        }
        this.setState({
            errors: errors
        });
        return isValid
    };


    componentDidMount() {
        this.setState({
            newImage: 'product-01.jpg'
        })
    }

    //function for preview images
    changePreviewImage(image) {
        this.setState({
            newImage: image
        });
    }

    onChangeInput = (name, e) => {
        onChangeValue(this, name, e);
    };

    getBase64 = (file, callback) => {
        console.log(file);
        let reader = new FileReader();
        reader.readAsDataURL(file[0]);
        reader.onloadend = async function () {
            callback(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    };

    ImageChange = (index, picture) => {
        if (!picture || picture.length < 1) {
            return;
        }
        if (picture === '') {
            this.setState({
                ...this.state,
                ErrorMsg: "Định dạng file không hỗ trợ"
            })
        } else {
            this.getBase64(picture, async result => {
                const {pictures, picturesString} = this.state;
                pictures[index] = result;
                picturesString[index] = picture[0].name;
                await this.setState(prevState => ({
                    pictures: pictures,
                    picturesString: picturesString,
                    ErrorMsg: ''
                }));
            })
        }
    };
    getListImage = async (_callBack) => {
        let lstImage = [];
        const {pictures, picturesString} = this.state;
        for (let i = 0; i < pictures.length; i++) {
            lstImage.push({
                imageData: pictures[i],
                name: picturesString[i]
            })
        }
        console.log("lstImage", lstImage)
        _callBack(lstImage)
    };

    onChangeCheckBox = (name, e) => {
        if (e.target.name) {
            let lstValue = JSON.parse(JSON.stringify(this.state.data[name]));
            if (lstValue && lstValue.some(item => item === e.target.name)) {
                lstValue = lstValue.filter(item => item !== e.target.name);
            } else {
                lstValue.push(e.target.name);
            }
            onChangeSelect(this, name, lstValue);
        }
    };

    onSubmit = async () => {
        let dataRequest = JSON.parse(JSON.stringify(this.state.data));
        dataRequest.categoryString = JSON.stringify(dataRequest.categoryString);
        dataRequest.colorsString = JSON.stringify(dataRequest.colorsString);
        dataRequest.sizeString = JSON.stringify(dataRequest.sizeString);
        dataRequest.salePrice = this.state.data.price;
        dataRequest.tagsString = dataRequest.categoryString;
        dataRequest.picturesString = JSON.stringify(this.state.picturesString);
        dataRequest.variantsString = JSON.stringify(dataRequest.variantsString);
        await this.getListImage(result => {
            dataRequest.lstImages = result;
        });
        if (this.validate()) {
            this.serviceProduct.update(dataRequest, () => {
                showMessage('Sửa sản phẩm thành công');
                window.open("/admin/Product", "_self");
            })
        }
    };

    render() {
        const {photoIndex, isOpen, size, colors, tags, pictures} = this.state;
        const {product} = this.props;
        const images = [];
        {
            product.pictures.map((pic) =>
                images.push(pic)
            )
        }
        return (
            <section>
                <div className="product-content-top single-product single-product-edit">
                    <Row>
                        <div className="product-top-left col-xl-5 col-md-6">
                            <div className="product-top-left-inner">
                                <div className="ciyashop-product-images">
                                    <div
                                        className="ciyashop-product-images-wrapper ciyashop-gallery-style-default ciyashop-gallery-thumb_position-bottom ciyashop-gallery-thumb_vh-horizontal">
                                        <div
                                            className="ciyashop-product-gallery ciyashop-product-gallery--with-images slick-carousel">
                                            <Slider {...settings}
                                                    className="ciyashop-product-gallery__wrapper popup-gallery">
                                                <div className="ciyashop-product-gallery__image">
                                                    <img src={product.pictures[0]} className="img-fluid" alt={""}/>
                                                </div>
                                            </Slider>
                                            <div className="ciyashop-product-gallery_buttons_wrapper">
                                                <div
                                                    className="ciyashop-product-gallery_button ciyashop-product-gallery_button-zoom popup-gallery"
                                                    onClick={() => this.setState({isOpen: true})}>
                                                    <Link to="#" className="ciyashop-product-gallery_button-link-zoom">
                                                        <i className="fa fa-arrows-alt"/>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="ciyashop-product-thumbnails">
                                            <Slider {...productslider} className="ciyashop-product-thumbnails__wrapper">
                                                {product.pictures.map((pictureimage, index) =>
                                                    <div key={index}>
                                                        <div className="ciyashop-product-thumbnail__image">
                                                            <a href="javascript:void(0)">
                                                                <img src={pictureimage} className="img-fluid" alt={""}/>
                                                            </a>
                                                            <div
                                                                className="d-flex justify-content-center image-content align-items-center">
                                                                <ImageUploader
                                                                    buttonText=""
                                                                    onChange={this.ImageChange.bind(this, index)}
                                                                    withPreview
                                                                    withIcon={false}
                                                                    maxFileSize={5242880}
                                                                    imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                                                />
                                                            </div>
                                                        </div>

                                                    </div>
                                                )}
                                            </Slider>
                                        </div>
                                        <div className="clearfix"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="product-top-right col-xl-7 col-md-6">
                            <div className="product-top-right-inner">
                                <div className="summary entry-summary">
                                    <FormGroup className="edit-icon">
                                        <Input type="text"
                                               onChange={this.onChangeInput.bind(this, 'name')}
                                               className="form-control product_title"
                                               placeholder="Product Name" defaultValue={product.name}/>
                                        <span hidden={!this.state.errors['name']}
                                              className={'errors'}>{this.state.errors['name']}</span>
                                    </FormGroup>
                                    <FormGroup className="edit-icon">
                                        <Input type="number"
                                               className="form-control price" placeholder="Product Price"
                                               onChange={this.onChangeInput.bind(this, 'price')}
                                               defaultValue={`${product.price}`}/>
                                        <span hidden={!this.state.errors['price']}
                                              className={'errors'}>{this.state.errors['price']}</span>
                                    </FormGroup>
                                    <FormGroup className="edit-icon">
                                        <Input type="textarea" className="form-control" rows="3"
                                               onChange={this.onChangeInput.bind(this, 'description')}
                                               placeholder="Product Description"
                                               defaultValue={product.description}/>
                                    </FormGroup>
                                    <FormGroup className="edit-icon">
                                        <Input type="textarea" className="form-control" rows="3"
                                               onChange={this.onChangeInput.bind(this, 'shortDetails')}
                                               placeholder="Mô tả ngắn"/>
                                    </FormGroup>
                                    <Label className="title">Kích cỡ</Label>
                                    <FormGroup>
                                        {size && size.map((size, index) =>
                                            <Label key={index}>
                                                <Input key={index} type="checkbox"
                                                       name={size.size}
                                                       checked={this.state.data.sizeString.some(item => item === size.size)}
                                                       onChange={this.onChangeCheckBox.bind(this, 'sizeString')}/>{' '}
                                                {size.size}
                                            </Label>
                                        )}
                                        <span hidden={!this.state.errors['sizeString']}
                                              className={'errors'}>{this.state.errors['sizeString']}</span>
                                    </FormGroup>
                                    <Label className="title">Màu sắc</Label>
                                    <FormGroup>
                                        {colors && colors.map((color, index) =>
                                            <Label key={index}>
                                                <Input
                                                    key={index}
                                                    name={color.color}
                                                    checked={this.state.data.colorsString.some(item => item === color.color)}
                                                    onChange={this.onChangeCheckBox.bind(this, 'colorsString')}
                                                    type="checkbox"/>{' '}
                                                {color.color}
                                            </Label>
                                        )}
                                        <span hidden={!this.state.errors['colorsString']}
                                              className={'errors'}>{this.state.errors['colorsString']}</span>
                                    </FormGroup>
                                    <Label className="title">Danh Mục</Label>
                                    <FormGroup>
                                        {tags && tags.map((category, index) =>
                                            <Label key={index}>
                                                <Input
                                                    key={index}
                                                    name={category.categoryName}
                                                    checked={this.state.data.categoryString.some(item => item === category.categoryName)}
                                                    onChange={this.onChangeCheckBox.bind(this, 'categoryString')}
                                                    type="checkbox"/>{' '}
                                                {category.categoryName}
                                            </Label>
                                        )}
                                        <span hidden={!this.state.errors['categoryString']}
                                              className={'errors'}>{this.state.errors['categoryString']}</span>
                                    </FormGroup>
                                    <FormGroup>
                                        <Label className="title pl-0">Số lượng</Label>
                                        <Input type="text"
                                               className="form-control"
                                               defaultValue={product.quantity}
                                               onChange={this.onChangeInput.bind(this, 'quantity')}
                                               placeholder="Số lượng"/>
                                        <span hidden={!this.state.errors['quantity']}
                                              className={'errors'}>{this.state.errors['quantity']}</span>
                                    </FormGroup>
                                    <a href="#" onClick={this.onSubmit} className="btn btn-primary mb-2 mr-2"> Cập
                                        nhật </a>
                                    <Link to="/admin/Product" className="btn btn-danger mb-2"> Đóng </Link>
                                </div>
                            </div>
                        </div>
                    </Row>
                </div>
                <div>
                    {isOpen && (
                        <Lightbox
                            mainSrc={pictures[photoIndex]}
                            nextSrc={pictures[(photoIndex + 1) % pictures.length]}
                            prevSrc={pictures[(photoIndex + pictures.length - 1) % pictures.length]}
                            onCloseRequest={() => this.setState({isOpen: false, indexPic: photoIndex})}
                            onMovePrevRequest={() =>
                                this.setState({
                                    photoIndex: (photoIndex + pictures.length - 1) % pictures.length,
                                })
                            }
                            onMoveNextRequest={() =>
                                this.setState({
                                    photoIndex: (photoIndex + 1) % pictures.length,
                                })
                            }
                        />
                    )}
                </div>
            </section>
        )
    }
}

export default ProductEditDetail;

