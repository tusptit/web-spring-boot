/**
 *  Admin Site Product Edit Page
 */
import React, {Component} from 'react';
import {Container} from 'reactstrap';
import ProductEditDetail from '../ProductEditDetail';
import ProductService from "../../services/ProductService";


class Productedit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ProductId: parseInt(this.props.match.params.id),
        };
        this.service = new ProductService();
    }

    getProduct = async () => {
        await this.service.getOne({
            id: parseInt(this.props.match.params.id)
        }, async result => {
            this.setState({
                product: result.data
            })
        });
    };
    UNSAFE_componentWillMount = async () => {
        await this.getProduct();
    };

    render() {
        const Productedit = this.state.product;
        return (
            <div>
                {Productedit ?
                    <div className="site-content">
                        <div className="content-wrapper section-ptb">
                            <Container>
                                <ProductEditDetail product={Productedit}/>
                            </Container>
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

export default Productedit;
